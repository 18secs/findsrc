findsrc
=======
uglify 된 소스의 위치 값으로 uglify 되지 않은 소스의 해당 위치로 vim editor 를 실행합니다.


Install
=======
```
$ npm install -g https://gitlab.com/18secs/findsrc.git
```


Usage
=====

```
$ findsrc <source-map-filename>:<line>:<column>
```

Requirement
========
- node >= 8.x
- vim

